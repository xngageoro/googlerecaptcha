<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\Form\Type;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoogleRecaptchaV3Type extends AbstractGoogleRecaptchaType
{
    public const DEFAULT_ACTION_NAME = 'form';

    /** @var bool */
    private $hideBadge;

    private ConfigManager $configManager;

    /**
     * XngageGoogleRecaptchaV3Type constructor.
     *
     * @param string $publicKey
     * @param bool $enabled
     * @param bool   $hideBadge
     * @param string $apiHost
     */
    public function __construct(string $publicKey, bool $enabled, bool $hideBadge,
                                string $apiHost = 'www.google.com', ConfigManager $configManager)
    {
        $this->configManager = $configManager;
        $publicKey = $configManager->get('xngage_recaptcha.public_key');
        parent::__construct($publicKey, $enabled, $apiHost);

        $this->hideBadge = $hideBadge;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => false,
            'mapped' => false,
            'validation_groups' => ['Default'],
            'script_nonce_csp' => '',
            'action_name' => 'form',
        ]);

        $resolver->setAllowedTypes('script_nonce_csp', 'string');
        $resolver->setAllowedTypes('action_name', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return HiddenType::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function addCustomVars(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars = array_replace($view->vars, [
            'google_recaptcha_hide_badge' => $this->hideBadge,
            'script_nonce_csp' => $options['script_nonce_csp'] ?? '',
            'action_name' => $options['action_name'] ?? '',
        ]);
    }
}
