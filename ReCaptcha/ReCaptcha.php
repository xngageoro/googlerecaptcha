<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\ReCaptcha;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use ReCaptcha\ReCaptcha as ParentClass;
use ReCaptcha\RequestMethod;

class ReCaptcha extends ParentClass
{
    private ConfigManager $configManager;

    public function __construct($secret, RequestMethod $requestMethod = null,
                                ConfigManager $configManager)
    {
        $this->configManager = $configManager;

        $secret = $configManager->get('xngage_recaptcha.private_key');

        parent::__construct($secret, $requestMethod);
    }

}