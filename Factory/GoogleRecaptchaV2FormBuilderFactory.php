<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\Factory;

use Xngage\Bundle\GoogleRecaptchaBundle\Form\Type\GoogleRecaptchaType;
use Xngage\Bundle\GoogleRecaptchaBundle\Validator\Constraints\IsTrue;
use Symfony\Component\Form\FormFactoryInterface;

class GoogleRecaptchaV2FormBuilderFactory
{
    private $builder;

    public function __construct(FormFactoryInterface $builder)
    {
        $this->builder = $builder;
    }

    public function get(array $options = array())
    {
        $constraint = array(
            'constraints' => array(
                new IsTrue(),
            ),
        );

        return $this->builder->createBuilder(GoogleRecaptchaType::class, null, array_merge($options, $constraint));
    }
}
