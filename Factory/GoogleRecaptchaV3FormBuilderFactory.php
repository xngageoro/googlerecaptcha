<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\Factory;

use Xngage\Bundle\GoogleRecaptchaBundle\Form\Type\GoogleRecaptchaV3Type;
use Xngage\Bundle\GoogleRecaptchaBundle\Validator\Constraints\IsTrueV3;
use Symfony\Component\Form\FormFactoryInterface;

class GoogleRecaptchaV3FormBuilderFactory
{
    private $builder;

    public function __construct(FormFactoryInterface $builder)
    {
        $this->builder = $builder;
    }

    public function get(array $options = array())
    {
        $constraint = array(
            'constraints' => array(
                new IsTrueV3(),
            ),
        );

        return $this->builder->createBuilder(GoogleRecaptchaV3Type::class, null, array_merge($options, $constraint));
    }
}
