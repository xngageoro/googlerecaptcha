<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\DependencyInjection;

use Xngage\Bundle\GoogleRecaptchaBundle\Factory\GoogleRecaptchaV2FormBuilderFactory;
use Xngage\Bundle\GoogleRecaptchaBundle\Factory\GoogleRecaptchaV3FormBuilderFactory;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 */
class XngageGoogleRecaptchaExtension extends Extension
{
    const ALIAS = 'xngage_google_recaptcha';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        foreach ($config as $key => $value) {
            $container->setParameter('xngage_google_recaptcha.'.$key, $value);
        }

        $this->registerWidget($container, $config['version']);

        if (null !== $config['http_proxy']['host'] && null !== $config['http_proxy']['port']) {
            $recaptchaService = $container->findDefinition('xngage_google_recaptcha.recaptcha');
            $recaptchaService->replaceArgument(1, new Reference('xngage_google_recaptcha.extension.recaptcha.request_method.proxy_post'));
        }

        if (3 === $config['version']) {
            $container->register('xngage_google_recaptcha.form_builder_factory', GoogleRecaptchaV3FormBuilderFactory::class)
                ->addArgument(new Reference('form.factory'));
        } else {
            $container->register('xngage_google_recaptcha.form_builder_factory', GoogleRecaptchaV2FormBuilderFactory::class)
                ->addArgument(new Reference('form.factory'));
        }

        foreach ($config['service_definition'] as $serviceDefinition) {
            $container->register('xngage_google_recaptcha.'.$serviceDefinition['service_name'], FormBuilderInterface::class)
                ->setFactory(array(
                    new Reference('xngage_google_recaptcha.form_builder_factory'),
                    'get',
                ))
                ->setArguments([$serviceDefinition['options']]);
        }
    }

    /**
     * Registers the form widget.
     *
     * @param ContainerBuilder $container
     */
    protected function registerWidget(ContainerBuilder $container, int $version = 2): void
    {
        $templatingEngines = $container->hasParameter('templating.engines')
            ? $container->getParameter('templating.engines')
            : array('twig');

        if (in_array('php', $templatingEngines, true)) {
            $formResource = 'XngageGoogleRecaptchaBundle:Form';

            $container->setParameter('templating.helper.form.resources', array_merge(
                $container->getParameter('templating.helper.form.resources'),
                array($formResource)
            ));
        }

        if (in_array('twig', $templatingEngines, true)) {
            $formResource = '@XngageGoogleRecaptcha/Form/google_recaptcha_widget.html.twig';
            if (3 === $version) {
                $formResource = '@XngageGoogleRecaptcha/Form/v3/google_recaptcha_widget.html.twig';
            }

            $container->setParameter('twig.form.resources', array_merge(
                $this->getTwigFormResources($container),
                array($formResource)
            ));
        }
    }

    private function getTwigFormResources(ContainerBuilder $container)
    {
        if (!$container->hasParameter('twig.form.resources')) {
            return [];
        }

        return $container->getParameter('twig.form.resources');
    }
}
