<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class IsTrue extends Constraint
{
    public $message = 'This value is not a valid captcha.';

    public $invalidHostMessage = 'The captcha was not resolved on the right domain.';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return Constraint::PROPERTY_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return 'xngage_google_recaptcha.true';
    }
}
