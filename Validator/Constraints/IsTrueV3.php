<?php

namespace Xngage\Bundle\GoogleRecaptchaBundle\Validator\Constraints;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class IsTrueV3 extends IsTrue
{
    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return 'google_recaptcha.v3.true';
    }
}
